# coding: utf-8

from __future__ import absolute_import

import os

from flask import json

from swagger_server.models.recipe_request import RecipeRequest  # noqa: E501
from swagger_server.models.recipe_response import RecipeResponse
from swagger_server.test import BaseTestCase


class TestConversionController(BaseTestCase):
    """ConversionController integration test stubs"""

    def test_basic_auth_wrong_pw(self):
        body = RecipeRequest(
            url="https://www.chefkoch.de/rezepte/1344331239198800/Saftiges-Vollkornbrot.html"
        )
        os.environ["BASIC_USER"] = "Aladdin"
        os.environ["BASIC_PASS"] = "do not open sesame"

        response = self.client.open(
            "/recipe",
            method="PUT",
            headers={"Authorization": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=="},
            data=json.dumps(body),
            content_type="application/json",
        )
        response_data = response.data.decode("utf-8")
        self.assert403(response, "Response body is : " + response_data)

    def test_basic_auth_wrong_user(self):
        body = RecipeRequest(
            url="https://www.chefkoch.de/rezepte/1344331239198800/Saftiges-Vollkornbrot.html"
        )
        os.environ["BASIC_USER"] = "aladdinn"
        os.environ["BASIC_PASS"] = "open sesame"

        response = self.client.open(
            "/recipe",
            method="PUT",
            headers={"Authorization": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=="},
            data=json.dumps(body),
            content_type="application/json",
        )
        response_data = response.data.decode("utf-8")
        self.assert403(response, "Response body is : " + response_data)

    def test_basic_auth_no_header(self):
        body = RecipeRequest(
            url="https://www.chefkoch.de/rezepte/1344331239198800/Saftiges-Vollkornbrot.html"
        )
        os.environ["BASIC_USER"] = "Aladdin"
        os.environ["BASIC_PASS"] = "do not open sesame"

        response = self.client.open(
            "/recipe",
            method="PUT",
            data=json.dumps(body),
            content_type="application/json",
        )
        response_data = response.data.decode("utf-8")
        self.assert401(response, "Response body is : " + response_data)

    def test_convert_recipe(self):
        """Test case for convert_recipe

        Request the conversion of a recipe
        """
        body = RecipeRequest(
            url="https://www.chefkoch.de/rezepte/1344331239198800/Saftiges-Vollkornbrot.html"
        )
        os.environ["BASIC_USER"] = "Aladdin"
        os.environ["BASIC_PASS"] = "open sesame"
        response = self.client.open(
            "/recipe",
            method="PUT",
            headers={"Authorization": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=="},
            data=json.dumps(body),
            content_type="application/json",
        )
        response_data = response.data.decode("utf-8")
        self.assert200(response, "Response body is : " + response_data)

        response_data = RecipeResponse.from_dict(json.loads(response_data))
        self.assertEqual(response_data.author, "egghead")
        self.assertEqual(response_data.cook_time, 60.0)
        self.assertEqual(
            response_data.image,
            "https://img.chefkoch-cdn.de/rezepte/1344331239198800/bilder/437561/crop-960x540/saftiges-vollkornbrot.jpg",
        )
        instructions = """Die Zutaten in der genannten Reihenfolge mischen und in der Küchenmaschine oder mit dem Rührgerät mit den Knethaken zu einem Teig verarbeiten. Dieser ist relativ flüssig. 

In eine mit Backpapier ausgelegt Kastenform (meine ist 30 cm lang und 15 cm breit) füllen. Wenn das Papier am Rand zerknüllt ist, macht das gar nichts. 

Dann die Form in den kalten (!) Backofen auf den Rost in die Mitte stellen und bei Ober-/Unterhitze 200°C eine Stunde backen. Das Brot ist sehr schnell zu machen, superlecker und lange saftig."""
        self.assertEqual(response_data.instructions, instructions)
        self.assertEqual(response_data.name, "Saftiges Vollkornbrot")
        self.assertEqual(response_data.prep_time, 10.0)
        self.assertEqual(response_data.total_time, 70.0)


if __name__ == "__main__":
    import unittest

    unittest.main()
