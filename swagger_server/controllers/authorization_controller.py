import os

from connexion.exceptions import Forbidden

"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""


def check_basicAuth(username, password, required_scopes=None):
    if "BASIC_USER" in os.environ:
        if username != os.environ["BASIC_USER"]:
            raise Forbidden()
    if "BASIC_PASS" in os.environ:
        if password != os.environ["BASIC_PASS"]:
            raise Forbidden()
    return {"allowed": True}
