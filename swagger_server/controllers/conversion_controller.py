import re
from typing import TypeVar

import connexion
from recipe_scrapers import scrape_me, WebsiteNotImplementedError

from swagger_server.models.recipe_request import RecipeRequest  # noqa: E501
from swagger_server.models.recipe_response import RecipeResponse

_url_regex = re.compile(
    r"https?://(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)"
)

T = TypeVar("T")


def _get(method, default: T) -> T:
    try:
        return method()
    except:
        return default


def _read_time(t: str):
    try:
        t = t.replace("P0DT", "").replace("M", "").split("H")
        return int(t[0]) * 60 + int(t[1])
    except:
        return -1


def convert_recipe(body):  # noqa: E501
    """Request the conversion of a recipe

     # noqa: E501

    :param body: Recipe URL
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = RecipeRequest.from_dict(connexion.request.get_json())  # noqa: E501
        target_url = body.url

        if not _url_regex.match(target_url):
            return "You did not supply a valid URL", 400

        try:
            try:
                result = scrape_me(body.url)
            except WebsiteNotImplementedError:
                result = scrape_me(body.url, wild_mode=True)
            except AttributeError as e:
                if isinstance(e.args, tuple) and len(e.args) > 0:
                    if "WebsiteNotImplementedError" in e.args[0]:
                        result = scrape_me(body.url, wild_mode=True)
                    else:
                        raise e
                else:
                    raise e

            prep_time = -1
            if "prepTime" in result.schema.data:
                prep_time = _read_time(result.schema.data["prepTime"])
            cook_time = -1
            if "cookTime" in result.schema.data:
                cook_time = _read_time(result.schema.data["cookTime"])
            rating_count = -1
            if (
                "aggregateRating" in result.schema.data
                and "ratingCount" in result.schema.data["aggregateRating"]
            ):
                rating_count = result.schema.data["aggregateRating"]["ratingCount"]
            rating_value = -1
            if (
                "aggregateRating" in result.schema.data
                and "ratingValue" in result.schema.data["aggregateRating"]
            ):
                rating_value = result.schema.data["aggregateRating"]["ratingValue"]

            result = RecipeResponse(
                _get(result.image, ""),
                _get(result.title, ""),
                _get(result.instructions, ""),
                prep_time,
                cook_time,
                _get(result.total_time, 0),
                rating_count,
                rating_value,
                _get(result.author, ""),
                _get(result.ingredients, []),
            )
            if len(result.ingredients) == 0 and result.instructions == "":
                return "Recipe could not be found", 404

            return result
        except AttributeError as e:
            if "NoSchemaFoundInWildMode" in e.args[0]:
                return "Have no parser, website has no schema.", 501
            else:
                raise e
        except Exception as e:
            return "Unhandled Problem", 500

    return "Please send a valid JSON request", 400
