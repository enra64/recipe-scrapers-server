# Recipe-Server

## Overview
This is a shim to use https://github.com/hhursev/recipe-scrapers as a server.

## Requirements
Python 3.8

## Usage
To run the server, please execute the following from the root directory:

```
pip3 install -r requirements.txt
python3 -m swagger_server
```

and open your browser to here:

```
http://localhost:8020/ui/
```

## Documentation
The REST endpoints are described in an OpenAPI 3.0 document that lives at `swagger_server/swagger/swagger.yaml`

## Running with Docker

To run the server on a Docker container, please execute the following from the root directory:

```bash
# building the image
docker build -t recipe-server .

# starting up a container
docker run -p 8020:8020 server
```